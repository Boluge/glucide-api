<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as'=>'home', function () {
    return view('welcome');
}]);

$GLOBALS = array(
    'auth'      => "auth",
    'admin'     => "g-admin",
    'food'      => "aliment",
    'component' => "element",
    'category'  => "categorie",
    'user'      => "utilisateur",
    'meal'      => "repas"
);

// Authentication routes...
Route::get($GLOBALS['auth'].'/login', [ 'as'=>'auth.getlogin' ,'uses' => 'Auth\AuthController@getLogin']);
Route::post($GLOBALS['auth'].'/login', [ 'as'=>'auth.postlogin' ,'uses' => 'Auth\AuthController@postLogin']);
Route::get($GLOBALS['auth'].'/logout', [ 'as'=>'auth.logout' ,'uses' => 'Auth\AuthController@getLogout']);

// Registration routes...
Route::get($GLOBALS['auth'].'/register', [ 'as'=>'auth.getregister' ,'uses' => 'Auth\AuthController@getRegister']);
Route::post($GLOBALS['auth'].'/register', [ 'as'=>'auth.postregister' ,'uses' => 'Auth\AuthController@postRegister']);

// Administrator
Route::group(['prefix'=> $GLOBALS['admin'], ['middleware' => 'auth']], function(){
    // Foods
    Route::get($GLOBALS['food'], [ 'as' => 'food.index', 'uses' => 'Admin\FoodController@index' ]);
    Route::get($GLOBALS['food'].'.json', [ 'as' => 'food.list', 'uses' => 'Admin\FoodController@json' ]);
    Route::get($GLOBALS['food'].'/create', [ 'as' => 'food.create', 'uses' => 'Admin\FoodController@create' ]);
    Route::post($GLOBALS['food'], [ 'as' => 'food.store', 'uses' => 'Admin\FoodController@store' ]);
    Route::get($GLOBALS['food'].'/{id}/edit', [ 'as' => 'food.edit', 'uses' => 'Admin\FoodController@edit' ])->where('id', '[0-9]+');
    Route::post($GLOBALS['food'].'/{id}/update', [ 'as' => 'food.update', 'uses' => 'Admin\FoodController@update' ])->where('id', '[0-9]+');
    Route::get($GLOBALS['food'].'/{id}/delete', [ 'as' => 'food.delete', 'uses' => 'Admin\FoodController@destroy' ])->where('id', '[0-9]+');

    // Food's category
    Route::get($GLOBALS['category'], [ 'as' => 'category.index', 'uses' => 'Admin\CategoryController@index' ]);
    Route::get($GLOBALS['category'].'/create', [ 'as' => 'category.create', 'uses' => 'Admin\CategoryController@create' ]);
    Route::post($GLOBALS['category'], [ 'as' => 'category.store', 'uses' => 'Admin\CategoryController@store' ]);
    Route::get($GLOBALS['category'].'/{id}/edit', [ 'as' => 'category.edit', 'uses' => 'Admin\CategoryController@edit' ])->where('id', '[0-9]+');
    Route::post($GLOBALS['category'].'/{id}/update', [ 'as' => 'category.update', 'uses' => 'Admin\CategoryController@update' ])->where('id', '[0-9]+');
    Route::get($GLOBALS['category'].'/{id}/delete', [ 'as' => 'category.delete', 'uses' => 'Admin\CategoryController@destroy' ])->where('id', '[0-9]+');

    // Users
    Route::get($GLOBALS['user'], [ 'as' => 'user.index', 'uses' => 'Admin\UserController@index' ]);
    Route::get($GLOBALS['user'].'/create', [ 'as' => 'user.create', 'uses' => 'Admin\UserController@create' ]);
    Route::get($GLOBALS['user'].'.json', [ 'as' => 'user.json', 'uses' => 'Admin\UserController@json' ]);
    Route::post($GLOBALS['user'], [ 'as' => 'user.store', 'uses' => 'Admin\UserController@store' ]);
    Route::get($GLOBALS['user'].'/{id}/edit', [ 'as' => 'user.edit', 'uses' => 'Admin\UserController@edit' ])->where('id', '[0-9]+');
    Route::post($GLOBALS['user'].'/{id}/update', [ 'as' => 'user.update', 'uses' => 'Admin\UserController@update' ])->where('id', '[0-9]+');
    Route::get($GLOBALS['user'].'/{id}/delete', [ 'as' => 'user.delete', 'uses' => 'Admin\UserController@destroy' ])->where('id', '[0-9]+');
});

Route::group([['middleware' => 'auth']], function(){

    // User Profile & Settings
    Route::get($GLOBALS['user'].'/{id}/profile', ['as' => 'profile', 'uses' => 'UserController@profile'])->where('id', '[0-9]+');
    Route::post($GLOBALS['user'].'/{id}/update', ['as' => 'profile.update', 'uses' => 'UserController@update' ])->where('id', '[0-9]+');

    // Meals
    Route::get($GLOBALS['meal'], [ 'as' => 'meal.index', 'uses' => 'MealController@index' ]);
    Route::get($GLOBALS['meal'].'.json', [ 'as' => 'meal.json', 'uses' => 'MealController@json' ]);
    Route::get($GLOBALS['meal'].'/create', [ 'as' => 'meal.create', 'uses' => 'MealController@create' ]);
    Route::post($GLOBALS['meal'], [ 'as' => 'meal.store', 'uses' => 'MealController@store' ]);
    Route::get($GLOBALS['meal'].'/{id}-{slug}', [ 'as' => 'meal.view', 'uses' => 'MealController@view' ])->where('id', '[0-9]+');
    Route::get($GLOBALS['meal'].'/{id}-{slug}/edit', [ 'as' => 'meal.edit', 'uses' => 'MealController@edit' ])->where('id', '[0-9]+');
    Route::post($GLOBALS['meal'].'/{id}-{slug}/update', [ 'as' => 'meal.update', 'uses' => 'MealController@update' ])->where('id', '[0-9]+');
    Route::get($GLOBALS['meal'].'/{id}-{slug}/delete', [ 'as' => 'meal.delete', 'uses' => 'MealController@destroy' ])->where('id', '[0-9]+');

    // Food Json
    Route::get($GLOBALS['food'].'s.json', [ 'as' => 'food.json', 'uses' => 'FoodController@json' ]);

    // Components
    Route::get($GLOBALS['component'].'/{idmeal}-{slugmeal}/create', [ 'as' => 'component.create', 'uses' => 'ComponentController@create' ])->where('idmeal', '[0-9]+');
    Route::post($GLOBALS['component'].'/{idmeal}-{slugmeal}', [ 'as' => 'component.store', 'uses' => 'ComponentController@store' ])->where('idmeal', '[0-9]+');
    Route::get($GLOBALS['component'].'/{id}/edit', [ 'as' => 'component.edit', 'uses' => 'ComponentController@edit' ])->where('id', '[0-9]+');
    Route::post($GLOBALS['component'].'/{id}/update', [ 'as' => 'component.update', 'uses' => 'ComponentController@update' ])->where('id', '[0-9]+');
    Route::get($GLOBALS['component'].'/{id}/delete', [ 'as' => 'component.delete', 'uses' => 'ComponentController@destroy' ])->where('id', '[0-9]+');
    Route::get($GLOBALS['component'].'/{idmeal}.json', [ 'as' => 'component.json', 'uses' => 'ComponentController@json' ])->where('idmeal', '[0-9]+');
});

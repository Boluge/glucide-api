<?php

namespace Glucide\Http\Controllers;

//use Glucide\Http\Controllers\Admin\CategoryController;
use Illuminate\Support\Facades\DB;

class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function json()
    {
      $foods = DB::table('foods')
        ->leftJoin('categories', 'categories.id', '=', 'foods.category_id')
        ->select('foods.id', 'foods.name', 'foods.slug', 'foods.sugar', 'foods.weight', 'categories.id as category_id', 'categories.name as category_name')
        ->orderBy('category_name')
        ->get();

      return response()->json($foods);
    }
}

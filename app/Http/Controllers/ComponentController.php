<?php

namespace Glucide\Http\Controllers;

use Glucide\Components;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ComponentController extends Controller
{
    public function json($idmeal)
    {
        $user= Auth::user();

        $components = DB::table('components')
            ->join('foods', 'foods.id', '=', 'components.food_id')
            ->join('categories', 'foods.category_id', '=', 'categories.id')
            ->join('meals', 'components.meal_id', '=', 'meals.id')
            ->select(
                [
                    'components.id AS id',
                    'components.created_at AS created',
                    'foods.name',
                    'foods.sugar',
                    'foods.weight',
                    'components.quantity',
                    'categories.name AS category',
                    DB::raw(env('DB_PREFIX').'foods.sugar*'.env('DB_PREFIX').'components.quantity AS sugar_quantity')
                ]
            )
            ->where('meals.user_id', '=',  $user['id'])
            ->where('meals.id', '=',  $idmeal)
            ->orderBy('created', 'asc')
            ->get();

        return response()->json($components);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request, $idmeal, $slugmeal)
    {
        $validator = Validator::make($request->all(), [
            'food_id' => 'required|numeric',
            'quantity' => 'required|numeric',
        ]);

        $parameters = $request->except(['_token']);

        if ($validator->fails()) {
            return redirect()
                ->route('meal.view',['idmeal' => $idmeal, 'slugmeal' => $slugmeal])
                ->withErrors($validator)
                ->withInput();
        }

        $component = new Components();
        $component->food_id = $parameters['food_id'];
        $component->meal_id = $idmeal;
        $component->quantity = $parameters['quantity'];

        $component->save();

        return redirect()
            ->route('meal.view',['idmeal' => $idmeal, 'slugmeal' => $slugmeal])
            ->with('success', 'Item was added !');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}

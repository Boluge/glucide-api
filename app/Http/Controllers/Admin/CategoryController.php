<?php

namespace Glucide\Http\Controllers\Admin;

use Glucide\Categories;
use Illuminate\Http\Request;

use Glucide\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if ( Auth::user()->roles != 'superadmin' ) {
            return App::abort(403, 'You are not authorized.');
        }

        $categories = DB::table('categories')->get();
        return view('foodcategories/index')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if ( Auth::user()->roles != 'superadmin' ) {
            return App::abort(403, 'You are not authorized.');
        }
        return view('foodcategories/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        if ( Auth::user()->roles != 'superadmin' ) {
            return App::abort(403, 'You are not authorized.');
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:foods|max:255',
            'slug' => 'max:255'
        ]);

        if ($validator->fails()) {
            return redirect()->route('category.create')
                ->withErrors($validator)
                ->withInput();
        }

        $parameters = $request->except(['_token']);

        if( empty($parameters['slug']) ){
            $parameters['slug'] = Str::slug($parameters['name']);
        }

        $category = new Categories();
        $category->name = $parameters['name'];
        $category->slug = $parameters['slug'];

        $category->save();

        return redirect()->route('category.index')->with('success', 'Item was added !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if ( Auth::user()->roles != 'superadmin' ) {
            return App::abort(403, 'You are not authorized.');
        }
        $category = Categories::find($id);
        return view('foodcategories/create')->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if ( Auth::user()->roles != 'superadmin' ) {
            return App::abort(403, 'You are not authorized.');
        }
        $category = Categories::find($id);
        $parameters = $request->except(['_token']);

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:foods|max:255'
        ]);

        if ($validator->fails()) {
            return redirect()->route('food.create')
                ->withErrors($validator)
                ->withInput();
        }

        if( empty($parameters['slug']) ){
            $parameters['slug'] = Str::slug($parameters['name']);
        }

        $category->name = $parameters['name'];
        $category->slug = $parameters['slug'];

        $category->save();

        return redirect()->route('category.index')->with('success', 'Category was updated !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if ( Auth::user()->roles != 'superadmin' ) {
            return App::abort(403, 'You are not authorized.');
        }
    }
}

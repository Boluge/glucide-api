<?php

namespace Glucide\Http\Controllers\Admin;

use Glucide\User;

use Glucide\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if ( Auth::user()->roles != 'superadmin' ) {
            return App::abort(403, 'You are not authorized.');
        }

        return view('users/index');
    }

    public function json()
    {
        if ( Auth::user()->roles != 'superadmin' ) {
            return App::abort(403, 'You are not authorized.');
        }
        
        $users = DB::table('users')
                    ->select('id', 'name', 'firstname', 'email', 'created_at', 'roles', 'gravatar')
                    ->get();

        return response()->json($users);
    }
}

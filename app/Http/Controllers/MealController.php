<?php

namespace Glucide\Http\Controllers;

use Glucide\Components;
use Glucide\Meals;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class MealController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('meals/index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function json()
    {
        $user_id = Auth::user()->id;
        $meals = Meals::where('user_id','=', $user_id)->orderBy('created_at', 'desc')->get();

        return response()->json($meals);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function mealJson($id)
    {
        $components = Components::where('meal_id','=', $id)->get();
        return response()->json($components);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function view($id, $slug)
    {
        $user= Auth::user();
        $meal = Meals::select('*')->where('id', '=', $id)->where('slug','=', $slug)->where('user_id','=', $user['id'])->first();

        $corrective = $user['corrective'];

        if($meal['blood_sugar'] > 1.4){
            $hyper = $meal['blood_sugar'] - 1.2;
            $raw = ($hyper*$corrective)/0.5;
            $meal['corrective'] = floor($raw * 2) / 2;
        } else if ($meal['blood_sugar'] < 0.6) {
            $meal['corrective'] = 'Hypo';
        } else {
            $meal['corrective'] = 0;
        }

        return view('meals/view')->with('meal', $meal);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('meals/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'blood_sugar' => 'numeric|max:8',
        ]);

        $parameters = $request->except(['_token']);

        if ($validator->fails()) {
            return redirect()->route('meal.create')
                ->withErrors($validator)
                ->withInput();
        }

        if( empty($parameters['slug']) ){
            $parameters['slug'] = Str::slug($parameters['name']);
        }

        $meal = new Meals();
        $meal->name = $parameters['name'];
        $meal->slug = $parameters['slug'];
        $meal->user_id = Auth::user()->id;
        $meal->blood_sugar = $parameters['blood_sugar'];

        $meal->save();
        return redirect()->route('meal.view', ['id' => $meal->id, 'slug' => $meal->slug])->with('success', 'Nouveau repas créé !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $meal = Meals::find($id);
        $meal->delete();

        return redirect()->route('meal.index')->with('success', 'Le repas à été supprimé !');
    }
}

import Vue from 'vue'
import Resource from 'vue-resource'

import MealsContainer from './vue/MealsContainer.vue'
import UsersContainer from './vue/UsersContainer.vue'
import Modal from './vue/Modal.vue'

/* eslint-disable no-new */
Vue.use(Resource)
new Vue({
  el: 'body',
  components: {
    mealscontainer: MealsContainer,
    userscontainer: UsersContainer,
    modal: Modal
  }
})

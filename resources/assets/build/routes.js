(function () {

    var laroute = (function () {

        var routes = {

            absolute: false,
            rootUrl: 'http://localhost:8000',
            routes : [{"host":null,"methods":["GET","HEAD"],"uri":"\/","name":"home","action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"auth\/login","name":"auth.getlogin","action":"Glucide\Http\Controllers\Auth\AuthController@getLogin"},{"host":null,"methods":["POST"],"uri":"auth\/login","name":"auth.postlogin","action":"Glucide\Http\Controllers\Auth\AuthController@postLogin"},{"host":null,"methods":["GET","HEAD"],"uri":"auth\/logout","name":"auth.logout","action":"Glucide\Http\Controllers\Auth\AuthController@getLogout"},{"host":null,"methods":["GET","HEAD"],"uri":"auth\/register","name":"auth.getregister","action":"Glucide\Http\Controllers\Auth\AuthController@getRegister"},{"host":null,"methods":["POST"],"uri":"auth\/register","name":"auth.postregister","action":"Glucide\Http\Controllers\Auth\AuthController@postRegister"},{"host":null,"methods":["GET","HEAD"],"uri":"g-admin\/aliment","name":"food.index","action":"Glucide\Http\Controllers\Admin\FoodController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"g-admin\/aliment\/create","name":"food.create","action":"Glucide\Http\Controllers\Admin\FoodController@create"},{"host":null,"methods":["POST"],"uri":"g-admin\/aliment","name":"food.store","action":"Glucide\Http\Controllers\Admin\FoodController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"g-admin\/aliment\/{id}\/edit","name":"food.edit","action":"Glucide\Http\Controllers\Admin\FoodController@edit"},{"host":null,"methods":["POST"],"uri":"g-admin\/aliment\/{id}\/update","name":"food.update","action":"Glucide\Http\Controllers\Admin\FoodController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"g-admin\/aliment\/{id}\/delete","name":"food.delete","action":"Glucide\Http\Controllers\Admin\FoodController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"g-admin\/categorie","name":"category.index","action":"Glucide\Http\Controllers\Admin\CategoryController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"g-admin\/categorie\/create","name":"category.create","action":"Glucide\Http\Controllers\Admin\CategoryController@create"},{"host":null,"methods":["POST"],"uri":"g-admin\/categorie","name":"category.store","action":"Glucide\Http\Controllers\Admin\CategoryController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"g-admin\/categorie\/{id}\/edit","name":"category.edit","action":"Glucide\Http\Controllers\Admin\CategoryController@edit"},{"host":null,"methods":["POST"],"uri":"g-admin\/categorie\/{id}\/update","name":"category.update","action":"Glucide\Http\Controllers\Admin\CategoryController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"g-admin\/categorie\/{id}\/delete","name":"category.delete","action":"Glucide\Http\Controllers\Admin\CategoryController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"g-admin\/utilisateur","name":"user.index","action":"Glucide\Http\Controllers\Admin\UserController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"g-admin\/utilisateur\/create","name":"user.create","action":"Glucide\Http\Controllers\Admin\UserController@create"},{"host":null,"methods":["GET","HEAD"],"uri":"g-admin\/utilisateur.json","name":"user.json","action":"Glucide\Http\Controllers\Admin\UserController@json"},{"host":null,"methods":["POST"],"uri":"g-admin\/utilisateur","name":"user.store","action":"Glucide\Http\Controllers\Admin\UserController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"g-admin\/utilisateur\/{id}\/edit","name":"user.edit","action":"Glucide\Http\Controllers\Admin\UserController@edit"},{"host":null,"methods":["POST"],"uri":"g-admin\/utilisateur\/{id}\/update","name":"user.update","action":"Glucide\Http\Controllers\Admin\UserController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"g-admin\/utilisateur\/{id}\/delete","name":"user.delete","action":"Glucide\Http\Controllers\Admin\UserController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"utilisateur\/{id}\/profile","name":"profile","action":"Glucide\Http\Controllers\UserController@profile"},{"host":null,"methods":["POST"],"uri":"utilisateur\/{id}\/update","name":"profile.update","action":"Glucide\Http\Controllers\UserController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"repas","name":"meal.index","action":"Glucide\Http\Controllers\MealController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"repas.json","name":"meal.json","action":"Glucide\Http\Controllers\MealController@json"},{"host":null,"methods":["GET","HEAD"],"uri":"repas\/create","name":"meal.create","action":"Glucide\Http\Controllers\MealController@create"},{"host":null,"methods":["POST"],"uri":"repas","name":"meal.store","action":"Glucide\Http\Controllers\MealController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"repas\/{id}-{slug}","name":"meal.view","action":"Glucide\Http\Controllers\MealController@view"},{"host":null,"methods":["GET","HEAD"],"uri":"repas\/{id}-{slug}\/edit","name":"meal.edit","action":"Glucide\Http\Controllers\MealController@edit"},{"host":null,"methods":["POST"],"uri":"repas\/{id}-{slug}\/update","name":"meal.update","action":"Glucide\Http\Controllers\MealController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"repas\/{id}-{slug}\/delete","name":"meal.delete","action":"Glucide\Http\Controllers\MealController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"aliments.json","name":"food.json","action":"Glucide\Http\Controllers\FoodController@json"},{"host":null,"methods":["GET","HEAD"],"uri":"element\/{idmeal}-{slugmeal}\/create","name":"component.create","action":"Glucide\Http\Controllers\ComponentController@create"},{"host":null,"methods":["POST"],"uri":"element\/{idmeal}-{slugmeal}","name":"component.store","action":"Glucide\Http\Controllers\ComponentController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"element\/{id}\/edit","name":"component.edit","action":"Glucide\Http\Controllers\ComponentController@edit"},{"host":null,"methods":["POST"],"uri":"element\/{id}\/update","name":"component.update","action":"Glucide\Http\Controllers\ComponentController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"element\/{id}\/delete","name":"component.delete","action":"Glucide\Http\Controllers\ComponentController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"element\/{idmeal}.json","name":"component.json","action":"Glucide\Http\Controllers\ComponentController@json"}],
            prefix: '',

            route : function (name, parameters, route) {
                route = route || this.getByName(name);

                if ( ! route ) {
                    return undefined;
                }

                return this.toRoute(route, parameters);
            },

            url: function (url, parameters) {
                parameters = parameters || [];

                var uri = url + '/' + parameters.join('/');

                return this.getCorrectUrl(uri);
            },

            toRoute : function (route, parameters) {
                var uri = this.replaceNamedParameters(route.uri, parameters);
                var qs  = this.getRouteQueryString(parameters);

                return this.getCorrectUrl(uri + qs);
            },

            replaceNamedParameters : function (uri, parameters) {
                uri = uri.replace(/\{(.*?)\??\}/g, function(match, key) {
                    if (parameters.hasOwnProperty(key)) {
                        var value = parameters[key];
                        delete parameters[key];
                        return value;
                    } else {
                        return match;
                    }
                });

                // Strip out any optional parameters that were not given
                uri = uri.replace(/\/\{.*?\?\}/g, '');

                return uri;
            },

            getRouteQueryString : function (parameters) {
                var qs = [];
                for (var key in parameters) {
                    if (parameters.hasOwnProperty(key)) {
                        qs.push(key + '=' + parameters[key]);
                    }
                }

                if (qs.length < 1) {
                    return '';
                }

                return '?' + qs.join('&');
            },

            getByName : function (name) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].name === name) {
                        return this.routes[key];
                    }
                }
            },

            getByAction : function(action) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].action === action) {
                        return this.routes[key];
                    }
                }
            },

            getCorrectUrl: function (uri) {
                var url = this.prefix + '/' + uri.replace(/^\/?/, '');

                if(!this.absolute)
                    return url;

                return this.rootUrl.replace('/\/?$/', '') + url;
            }
        };

        var getLinkAttributes = function(attributes) {
            if ( ! attributes) {
                return '';
            }

            var attrs = [];
            for (var key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    attrs.push(key + '="' + attributes[key] + '"');
                }
            }

            return attrs.join(' ');
        };

        var getHtmlLink = function (url, title, attributes) {
            title      = title || url;
            attributes = getLinkAttributes(attributes);

            return '<a href="' + url + '" ' + attributes + '>' + title + '</a>';
        };

        return {
            // Generate a url for a given controller action.
            // laroute.action('HomeController@getIndex', [params = {}])
            action : function (name, parameters) {
                parameters = parameters || {};

                return routes.route(name, parameters, routes.getByAction(name));
            },

            // Generate a url for a given named route.
            // laroute.route('routeName', [params = {}])
            route : function (route, parameters) {
                parameters = parameters || {};

                return routes.route(route, parameters);
            },

            // Generate a fully qualified URL to the given path.
            // laroute.route('url', [params = {}])
            url : function (route, parameters) {
                parameters = parameters || {};

                return routes.url(route, parameters);
            },

            // Generate a html link to the given url.
            // laroute.link_to('foo/bar', [title = url], [attributes = {}])
            link_to : function (url, title, attributes) {
                url = this.url(url);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given route.
            // laroute.link_to_route('route.name', [title=url], [parameters = {}], [attributes = {}])
            link_to_route : function (route, title, parameters, attributes) {
                var url = this.route(route, parameters);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given controller action.
            // laroute.link_to_action('HomeController@getIndex', [title=url], [parameters = {}], [attributes = {}])
            link_to_action : function(action, title, parameters, attributes) {
                var url = this.action(action, parameters);

                return getHtmlLink(url, title, attributes);
            }

        };

    }).call(this);

    /**
     * Expose the class either via AMD, CommonJS or the global object
     */
    if (typeof define === 'function' && define.amd) {
        define(function () {
            return laroute;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = laroute;
    }
    else {
        window.laroute = laroute;
    }

}).call(this);


@extends('templates/default')

@section('title')
    Connection
@endsection

@section('content')
  @if(Session::has('success'))
    {{ Session::get('success') }}
  @elseif(Session::has('error'))
   {{ Session::get('error') }}
  @endif

  <h1>Se Connecter</h1>
  <form role="form" method="POST" action="{{ route('auth.postlogin') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
    <label for="email">Email</label>
    <input type="email" placeholder="E-mail" name="email" value="{{ old('email') }}" required/>
    <br>
    <label for="password">Mot de passe</label>
    <input type="password" placeholder="Password" name="password" required/>
    <br>
    <input type="radio" name="remember">Se souvenir de moi</paper-toggle-button>
    <br>
    <input type="submit" value="Se Connecter"/>
  </form>
  <a href="{{ url('/password/email') }}">Mot de passe oublié</a>
@endsection

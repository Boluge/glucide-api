@extends('templates/default')

@section('title')
    Créer un compte
@endsection

@section('content')
  <h1>Création d'un compte</h1>
  <form role="form" method="POST" action="{{ url('/auth/register') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

      <label for="firstname">Prénom</label>
      <input type="text" id="firstname" name="firstname" value="{{ old('firstname') }}"/>
      <br>
      <label for="name">Nom</label>
      <input type="text" id="name" name="name" value="{{ old('name') }}"/>
      <br>
      <label for="email">E-Mail</label>
      <input type="email" id="email" name="email" value="{{ old('email') }}"/>
      <br>
      <label for="password">Password</label>
      <input type="password" id="password" name="password" />
      <br>
      <label for="password_confirmation">Confirm Password</label>
      <input type="password" id="password_confirmation" name="password_confirmation" />
      <br>
      <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">Register</button>

  </form>
@endsection

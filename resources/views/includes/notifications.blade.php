@if(Session::has('success'))
    <paper-toast id="success" text="{{ Session::get('success') }}"></paper-toast>
    <script>document.getElementById('success').show()</script>
@endif

@if (count($errors) > 0)
    <paper-dialog class="notification" id="error"  modal>
        <h2>Attention !</h2>
        <div>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <div class="buttons">
            <paper-button dialog-confirm autofocus>Ok</paper-button>
        </div>
    </paper-dialog>
    <script>document.getElementById('error').open()</script>
@endif

<!doctype html>
<!--[if lt IE 9]>      <html class="no-js old-config" lang="fr"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Glucide | @yield('title')</title>
    <meta name="description" content="fr">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}"/>
</head>
<body>
    <!--[if lt IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <[endif]-->
    <div class="background"></div>

    <div class="mask">
        <div class="bluredBackground">
        </div>
    </div>

    <div class="navigation">
      <a href="{{ route('home') }}" class="navigation-logo">
        <img src="{{ URL::asset('img/glucide.svg') }}" alt="Glucide">
      </a>
      @include('../includes/usermenu')
      @if(Auth::check())
        <?php $url = "http://www.gravatar.com/avatar/".md5( strtolower( trim( Auth::user()->email ) ) )."?d=identicon&s=50"; ?>
        <div class="navigation-user">
          <div class="navigation-user-gravatar">
            <img class="avatar-img" src="{{ URL::asset($url) }}" />
          </div>
          <div class="navigation-user-meals">
            <a class="navigation-user-meals-link icon-food" href="{{ route('home') }}"></a>
          </div>
          <div class="navigation-user-profil">
            <a class="navigation-user-profil-link icon-params" href="{{ route('profile', ['id' => Auth::user()->id]) }}"></a>
          </div>
          <div class="navigation-user-diconnect">
            <a class="navigation-user-diconnect-link icon-cog" href="{{ route('home') }}"></a>
          </div>
        </div>
      @endif
    </div>

    <div class="slider">
      @include('../includes/notifications')
    </div>

    <div class="container">
      @yield('content')
    </div>

    <!-- Scripts -->
    <script async src="{{ URL::asset('js/app.js') }}?v=1462255200"></script>
</body>
</html>

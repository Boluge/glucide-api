
@if( Auth::check() && Auth::user()->roles == 'administrator' || Auth::check() && Auth::user()->roles == 'superadmin' )
    <button id="admin-menu-lower-right" class="mdl-button mdl-js-button mdl-button--icon admin-button">
        <i class="material-icons">settings</i>
    </button>

    <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect user-menu" for="admin-menu-lower-right">
        @if(Auth::check() && Auth::user()->roles == 'superadmin')
            <li class="mdl-menu__item">
                <a href="{{ route('user.index') }}">
                    Show all Users
                </a>
            </li>
        @endif
        <li class="mdl-menu__item">
            <a href="{{ route('food.index') }}">
                Show all Foods
            </a>
        </li>
        @if(Auth::check() && Auth::user()->roles == 'superadmin')
            <li class="mdl-menu__item">
                <a href="{{ route('category.index') }}">
                    Show all Food's Categories
                </a>
            </li>
        @endif
    </ul>
@endif

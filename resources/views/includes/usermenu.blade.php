<ul class="navigation-mainmenu">
  <li class="navigation-mainmenu-item"></li>
@if(Auth::check())
  <li class="navigation-mainmenu-item">
    <a class="icon-food" href="{{ route('meal.index') }}">Mes Repas</a>
  </li>
  <li class="navigation-mainmenu-item">
    <a class="icon-params" href="{{ route('profile', ['id' => Auth::user()->id]) }}">Mon Profil</a>
  </li>
  @if( Auth::user()->roles != 'user' )
    @if( Auth::user()->roles != 'user' )
      <li class="navigation-mainmenu-item">
        <a class="icon-heart" href="{{ route('user.index') }}">Utilisateurs</a>
      </li>
      <li class="navigation-mainmenu-item">
        <a class="icon-star" href="{{ route('category.index') }}">Catégories</a>
      </li>
    @endif
    <li class="navigation-mainmenu-item">
      <a class="icon-cup" href="{{ route('food.index') }}">Aliments</a>
    </li>
  @endif
  <li class="navigation-mainmenu-item">
    <a class="icon-cog" href="{{ route('auth.logout') }}">Disconnect</a>
  </li>
@else
  <li class="navigation-mainmenu-item">
    <a class="icon-params" href="{{ route('auth.getlogin') }}">Se connecter</a>
  </li>
  <li class="navigation-mainmenu-item">
    <a class="icon-params" href="{{ route('auth.getregister') }}">Créer un compte</a>
  </li>
@endif
</ul>

@if(Auth::check())
    <a href="{{ route( 'profile', ['id' => Auth::user()->id] ) }}">Mon compte</a>
    <a href="{{ route('meal.index') }}">Mes repas</a>
    <a href="{{ route('auth.logout') }}">Se Déconnecter</a>
@else
    <a href="{{ route('auth.getlogin') }}">Se connecter</a>
@endif

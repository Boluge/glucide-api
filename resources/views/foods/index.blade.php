@extends('../templates/default')

@section('title')
    Tous les Aliments
@endsection

@section('content')
    <h1>Liste des Aliments</h1>
    <table class="table-foods">
        <thead>
          <tr>
              <th class="mdl-cell--hide-phone">Id</th>
              <th class="mdl-data-table__cell--non-numeric">Name</th>
              <th class="mdl-data-table__cell--non-numeric">Catégorie</th>
              <th>Edit</th>
          </tr>
        </thead>
        <tbody>
        @foreach($foods as $food)
            <tr>
                <td>{{ $food->id }}</td>
                <td>{{ $food->name }}</td>
                <td>{{ $food->categorie }}</td>
                <td>
                    <a href="{{ route('food.edit', ['id' => $food->id]) }}">
                        <i>Editer</i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

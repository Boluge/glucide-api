@extends('../templates/default')

@section('title')
    Edit your profile
@endsection

@section('content')
  <h1>Mon Profils</h1>

  <?php $url = "http://www.gravatar.com/avatar/".md5( strtolower( trim( $user->email ) ) )."?d=identicon&s=128"; ?>
  <img class="avatar-profile-img" src="{{ URL::asset($url) }}"/>

  <form method="post" class="mdl-grid" action="{{ route('profile.update',['id' => $user->id]) }}">

      <input type="hidden" name="_token" value="{{ csrf_token() }}">

      <div class="profil-profil">
          <input type="text" label="Prénom" name="firstname" value="{{ $user->firstname }}" required/>
          <input type="text" label="Nom" name="name" value="{{ $user->name }}" required/>
          <input type="email" label="E-mail" name="email" value="{{ $user->email }}" required/>
      </div>

      <div class="profil-settings">
          <input id="corrective" type="number" label="Corrective" step="0.05" min="0" name="corrective" value="{{ $user->corrective }}" required></input>
          <paper-tooltip for="corrective" offset="0">Unités d'insuline pour déscendre de 0.5g/l de sang</paper-tooltip>
          {{-- Unités d'insuline pour<br/>déscendre de 0.5g/l (1,25U) --}}
          <input id="prandial" type="number" label="Prandial" step="0.05" min="0" name="prandial" value="{{ $user->prandial }}" required></input>
          <paper-tooltip for="prandial" offset="0">Unités d'insuline pour de 10gr de glucides</paper-tooltip>
          {{-- Unités d'insuline pour<br/>de 10gr de glucides (2U) --}}
      </div>

      <div class="profil-password">
          <input type="password" label="Password" name="password"></input>
          <input type="password" label="Confirm Password" name="password_confirmation"></input>
      </div>

      <p>
          <input type="submit" value="Mettre à jour"/>
      </p>

  </form>
@endsection

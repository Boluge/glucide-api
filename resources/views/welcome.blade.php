@extends('templates/default')

@section('title')
    Home page
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="mdl-grid">
            <div class="card-notice card-notice-success mdl-shadow--6dp mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet">
                {{ Session::get('success') }}
            </div>
        </div>
    @elseif(Session::has('error'))
        <div class="mdl-grid">
            <div class="card-notice card-notice-error mdl-shadow--6dp mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet">
                {{ Session::get('error') }}
            </div>
        </div>
    @endif


    <h1>Welcome.blade</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid aspernatur consequatur dolore
        doloribus eveniet ipsam iure maiores minus modi, molestiae molestias officiis quam, qui quos suscipit
        temporibus ut vel.</p>
@endsection

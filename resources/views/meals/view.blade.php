@extends('../templates/default')

@section('title')
    {{ $meal->name }}
@endsection

@section('content')

    <h1 class="mdl-card__title-text">{{ $meal->name }}</h1>
    <p>{{ $meal->blood_sugar }} gr/L</p>

    <p>Correction de la glycémie : {{$meal->corrective}} u</p>

    {{-- Total de glucide du repas --}}
    {{-- Insuline pour corrigé la glycemie --}}
    {{-- Insuline pour le repas --}}
    {{-- Mettre en couleur la glycémie --}}

    {{--<a href="{{ route('component.create',['idmeal' => $meal->id, 'slugmeal' => $meal->slug ]) }}">Ajouter un aliment</a>--}}

    <h2>Lites des aliment composant votre repas</h2>

    <modal v-component="modal"></modal>

    <a href="{{ route('component.json', ['idmeal' => $meal->id ]) }}">{{ route('component.json', ['idmeal' => $meal->id ]) }}</a>
    <a href="{{ route('food.json') }}">Aliments</a>

@endsection
{{--@TODO créer une position pour recevoir la boite modal hors de content--}}
@extends('../templates/default')

@section('title')
    {{ isset($meal->id) ? 'Editer un repas' : 'Créer un repas' }}
@endsection

@section('content')
    <div class="mdl-grid">
        <h2 class="mdl-card__title-text">{{ isset($meal->id) ? 'Edition de repas' : 'Nouveau repas' }}</h2>

        <form method="post" action="{{ isset($meal->id) ? route('meal.update',['id' => $meal->id]) : route('meal.store') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <label for="name">Nom du repas</label>
            <input type="text" label="Nom du repas" id="name" name="name" value="{{ $meal->name or '' }}" required/>
            <br>
            <label for="blood_sugar">Glycémie</label>
            <input type="text" label="Glycemie" id="blood_sugar" name="blood_sugar" value="{{ $meal->blood_sugar or '' }}" required/>

            <p>
                <input type="submit" value="{{ isset($meal->id) ? 'Sauvegarder' : 'Continuer' }}"/>
            </p>
        </form>
    </div>
    @if(isset($food->id))
        <div class="mdl-card__menu">
            <a href="{{ route('meal.delete', ['id' => $meal->id]) }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
                <i class="material-icons">delete</i>
            </a>
        </div>
    @endif
@endsection

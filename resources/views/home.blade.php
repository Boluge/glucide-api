@extends('templates/default')

@section('title')
    Home page
@endsection

@section('content')
        @if(Session::has('success'))
            <div class="mdl-grid">
                <div class="card-notice card-notice-success mdl-shadow--6dp mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet">
                    {{ Session::get('success') }}
                </div>
            </div>
        @elseif(Session::has('error'))
            <div class="mdl-grid">
                <div class="card-notice card-notice-error mdl-shadow--6dp mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet">
                    {{ Session::get('error') }}
                </div>
            </div>
        @endif
        <div class="main-content">
            <my-work-space>
                <!-- Menu de ~74px -->
                <my-work-space-menu menu >
                    <my-menu-icon link="#" icon="icons:account-circle" tooltip="Compte"></my-menu-icon>
                    <my-menu-icon link="#" icon="maps:restaurant-menu" tooltip="Repas"></my-menu-icon>
                    <my-menu-icon link="#" icon="social:group" tooltip="Users"></my-menu-icon>
                    <my-menu-icon link="#" icon="social:cake" tooltip="Food"></my-menu-icon>
                </my-work-space-menu>

                <!-- Colonne de 400px -->
                <my-work-space-items items >
                    <my-meals-list></my-meals-list>
                </my-work-space-items>

                <!-- Colonne du reste - ~74px à droite -->
                <my-work-space-main main >
                    <h2>Main content</h2>
                </my-work-space-main>
            </my-work-space>
            <!--<my-meals-list></my-meals-list>-->
        </div>
@endsection

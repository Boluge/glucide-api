<?php

use Illuminate\Database\Seeder;
use Glucide\Foods;


class FoodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $import = new ImportCSV();

        $csvFile = public_path().'/csvs/foods.csv';
        $datas = $import->csvToArray($csvFile);

        DB::table('foods')->delete();

        foreach( $datas as $data ){
            Foods::create( $data );
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Glucide\Categories;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $import = new ImportCSV();

        $csvFile = public_path().'/csvs/categories.csv';
        $datas = $import->csvToArray($csvFile);

        DB::table('categories')->delete();

        foreach( $datas as $data ){
            Categories::create( $data );
        }
    }
}

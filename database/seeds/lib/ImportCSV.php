<?php

/**
 * Created by PhpStorm.
 * User: studio
 * Date: 15/11/2015
 * Time: 10:29
 */
class ImportCSV
{
    public function csvToArray($filename='', $delimiter=',')
    {
        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = NULL;
        $datas = array();
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                if(!$header)
                    $header = $row;
                else
                    $datas[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $datas;
    }
}

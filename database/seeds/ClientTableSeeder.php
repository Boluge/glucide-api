<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            'firstname'     => 'Stéphane',
            'name'          => 'Deluce',
            'email'         => 'boluge@gmail.com',
            'gravatar'      => md5('boluge@gmail.com'),
            'roles'         => 'superadmin',
            'password'      => bcrypt( env('ADMIN_MDP') ),
            'created_at'    => '2015-01-09 09:12:24',
            'corrective'    => '1',
            'prandial'      => '2',
        ]);

        DB::table('users')->insert([
            'firstname'     => 'Cerise',
            'name'          => 'Kirsh',
            'email'         => 'cerise@gmail.com',
            'gravatar'      => md5('cerise@gmail.com'),
            'password'      => bcrypt('cerise'),
            'created_at'    => '2016-04-09 09:25:36',
            'corrective'    => '1',
            'prandial'      => '2',
        ]);
    }
}
